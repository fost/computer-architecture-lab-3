import io
import os
import tempfile
import contextlib
import pytest
import json
from tools import CommandToJson, Offsets
import datapath

import control_unit
import translator


@pytest.mark.golden_test("test_data/integration/*.yml")
def test_integration(golden):

    with tempfile.TemporaryDirectory() as tmpdirname:
        source = os.path.join(tmpdirname, "source.fth")
        input_stream = os.path.join(tmpdirname, "input.txt")
        target = os.path.join(tmpdirname, "target.json")

        with open(source, "w", encoding="utf-8") as file:
            file.write(golden["source"])
        with open(input_stream, "w", encoding="utf-8") as file:
            file.write(golden["input"])

        with contextlib.redirect_stdout(io.StringIO()) as stdout:
            translator.main([source, target])
            control_unit.main([target, input_stream])

        with open(target, encoding="utf-8") as file:
            code = file.read()

        assert code == golden.out["code"]
        assert stdout.getvalue() == golden.out["output"]

@pytest.mark.golden_test("test_data/unit_tests/datapath/read_code.yml")
def test_datapath_unit(golden):
    with tempfile.TemporaryDirectory() as tmpdirname:
        source = os.path.join(tmpdirname, "source.fth")

        with open(source, "w", encoding="utf-8") as file:
            file.write(golden["source"])

        code = json.loads(golden.out["code"])
        instructions = list(map(lambda cmd: int(CommandToJson.from_json(cmd).opcode, 2), enumerate(code["program"])))

        memory = instructions + [0] * (Offsets.RO_DATA_OFFSET - len(instructions))
        ro_data = datapath.get_ro_data(code)

        memory += ro_data + [0] * (Offsets.MEMORY_GENERAL_SIZE - Offsets.RO_DATA_OFFSET - len(ro_data))

        data_path = datapath.DataPath([], code)

        assert data_path.memory == memory, "Unexpected memory contents!"
        assert data_path.program_counter == code["start_address"], "Start address differs!"
        assert data_path.stack == [], "Stack must be empty on start!"
        assert data_path.stack_pointer == -1, "Unexpected stack pointer!"

@pytest.mark.golden_test("test_data/unit_tests/datapath/throwing_*.yml")
def test_datapath_throwing(golden):
    code = json.loads(golden.out["code"])

    with pytest.raises(AssertionError) as exception:
        datapath.DataPath([], code)

    assert str(exception.value) == golden["error"]
