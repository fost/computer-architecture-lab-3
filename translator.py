import enum
import sys
import json

from tools import Command, JUMP_MASK, CommandToJson

symbol2command = {
    "halt": Command.HALT,
    "+": Command.ADD,
    ">": Command.GRT,
    "<": Command.LST,
    "=": Command.EQ,
    "swap": Command.SWAP,
    "last": Command.LAST,
    ".": Command.PRINT,
    "type": Command.PRINT_STR,
    "<<": Command.PUSH,
    ";": Command.RETURN,
    "dup_sec": Command.DUP_SEC,
    "dup": Command.DUP,
    "drop": Command.DROP,
    ":": None,
    "repeat": None,
    "begin": None,
    "while": None,
}


def is_push_arg(word):
    if word.isdigit() or (word[:2] == "S\"" and word[-1] == "\""):
        return True
    return False


def get_arg(word):
    if word.isdigit():
        return [int(word)]
    else:
        word_arr = word.encode()
        arr = [len(word)]
        arr.extend(word_arr)
        return arr


def dec_to_bin(number):
    return format(number, '010b')


def translate(source):
    program = []
    functions = {}
    with open(source, "r", encoding="utf-8") as source_file:
        code = source_file.readlines()
    offset, jmp_pos, jz_pos, ro_data_offset = -1, -1, -1, 0
    start_address = 0
    in_comment, in_function, push_string = False, False, False
    words_to_push = []
    for line in code:
        words = line.split()
        for word, next_word in zip(words, words[1:] + ['']):
            if word == "(":
                in_comment = True
                continue
            if word == ")":
                in_comment = False
                continue
            if word == "S\"":
                push_string = True
                continue
            if word == ":":
                assert not in_function, "Function inside function!"
                in_function = True
                continue
            if word == "begin":
                jmp_pos = offset + 1
                continue
            if in_comment or word == "\n":
                continue
            if push_string and word != "\"":
                words_to_push.append(word)
                continue
            if word not in symbol2command and in_function and word not in functions and not is_push_arg(word):
                functions[word] = offset + 1
                continue
            offset += 1
            if word == "\"":
                assert push_string, "Closing pushing string without opening it"
                push_string = False
                string_to_push = " ".join(words_to_push)
                program.append(
                    CommandToJson(dec_to_bin(Command.PUSH_ARG_STR + ro_data_offset),
                                  "push " + string_to_push, get_arg(string_to_push)))
                ro_data_offset += len(get_arg(string_to_push)) - 1
                continue
            if word == "repeat":
                program.append(CommandToJson(dec_to_bin(Command.JUMP + jmp_pos), "jmp " + str(jmp_pos)))
                program[jz_pos].opcode = dec_to_bin(Command.JZ + offset + 1)
                program[jz_pos].mnemonic = "jz " + str(offset + 1)
                continue
            if word == "while":
                program.append(CommandToJson(0, ""))
                jz_pos = offset
                continue
            if word == ";":
                in_function = False
                program.append(CommandToJson(dec_to_bin(Command.RETURN), "ret"))
                start_address = len(program)
                continue
            if word not in symbol2command and not is_push_arg(word):
                program.append(
                    CommandToJson(dec_to_bin(Command.CALL + functions[word]), "call " + str(functions[word])))
                continue
            if word not in symbol2command and is_push_arg(word):
                program.append(
                    CommandToJson(dec_to_bin(Command.PUSH_ARG + ro_data_offset), "push " + word, get_arg(word)))
                ro_data_offset += 1
                continue
            program.append(CommandToJson(dec_to_bin(symbol2command.get(word).value), word))
    program.append(CommandToJson(dec_to_bin(Command.HALT), "halt"))
    dict = {
        "program": list(map(lambda cmd: CommandToJson.get_json(cmd), enumerate(program))),
        "start_address": start_address
    }
    json_program = json.dumps(dict, indent=2)
    return json_program


def write_to_bin(target, json_program):
    with open(target, "w", encoding="utf-8") as target_file:
        target_file.write(json_program)


def read_instructions(filename):
    with open(filename, "r", encoding="utf-8") as instructions:
        json_instr = json.load(instructions)
    return json_instr


def main(args):
    assert len(args) == 2, \
        "Wrong arguments: translator.py <source_file> <target_file>"

    source, target = args
    json_program = translate(source)
    write_to_bin(target, json_program)


if __name__ == '__main__':
    main(sys.argv[1:])
