#!/usr/bin/python3

"""Модуль, куда вынесены общие элементы для Contol Unit и Data Path
"""

import enum

JUMP_MASK = int("1110000000", 2)
INSTR_ADDRESS_MASK = int("0001111111", 2)

class Offsets(enum.IntEnum):
    """Используются для относительной адресации
    """
    INSTRUCTIONS_OFFSET = 0
    RO_DATA_OFFSET = 100
    DATA_OFFSET = 150
    MEMORY_GENERAL_SIZE = 200


class Command(enum.IntEnum):
    """Команды, которые могут прийти в CU
    """
    HALT = 0
    ADD = 1
    GRT = 2
    LST = 3
    EQ = 4
    SWAP = 5
    LAST = 6
    PRINT = 7
    PRINT_STR = 8
    PUSH = 9
    RETURN = 10
    DUP_SEC = 11
    DUP = 12
    DROP = 13
    CALL = 128
    JZ = 256
    JUMP = 384
    PUSH_ARG = 512
    PUSH_ARG_STR = 640


class Signal(enum.IntEnum):
    """Сигналы, которые посылает CU в Data Path в зависимости от декодироанной команды
    """
    WRITE = 0
    CHECK_LS = 1
    CHECK_GRT = 2
    CHECK_EQ = 3
    CHECK_NOT_LAST = 4
    SWAP = 5
    ADD = 6
    READ_STR = 7
    READ = 8
    DUPLICATE_SECOND = 9
    DUPLICATE = 10
    WRITE_ARG_STR = 11
    WRITE_ARG_DIG = 12
    DROP = 13

class CommandToJson:
    def __init__(self, opcode, mnemonic, parameter=None, address=None):
        self.opcode = opcode
        self.mnemonic = mnemonic
        self.parameter = parameter
        self.address = address

    @classmethod
    def from_json(cls, json_entity_and_index):
        index, json_entity = json_entity_and_index
        return cls(json_entity["opcode"], json_entity["mnemonic"], json_entity["parameter"], json_entity["address"])

    @classmethod
    def get_json(cls, instance_and_index):
        index, entity = instance_and_index
        return {"address": index,
                "opcode": entity.opcode,
                "mnemonic": entity.mnemonic,
                "parameter": entity.parameter}


class HaltProgram(Exception):
    """Вызывается для остановки процессора
    """


class InputTokensEnd(Exception):
    """Вызывается для остановки считывания символов с потока
    """


class SegmentOverflow(Exception):
    """Вызывается для остановки программы, если выходим за пределы сегмента
    """

