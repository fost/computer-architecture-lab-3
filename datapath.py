#!/usr/bin/python3
# pylint: disable=missing-function-docstring
# pylint: disable=too-many-instance-attributes
# pylint: disable=too-many-branches

"""Модуль, где находится Data Path
"""
import logging

from tools import Signal, InputTokensEnd, SegmentOverflow, Offsets, CommandToJson


def get_ro_data(json_prog):
    return sum(
        filter(None,
               map(lambda param: param["parameter"], json_prog["program"])),
        []
    )


class DataPath:
    """ Тракт данных (пассивный), получает и обрабатывает сигналы от CU включая:
        ввод/вывод, память и арифметику.
    """

    def __init__(self, input_tokens, json_instructions):
        instructions = list(map(lambda cmd: int(CommandToJson.from_json(cmd).opcode, 2),
                                enumerate(json_instructions["program"])))
        assert Offsets.MEMORY_GENERAL_SIZE - Offsets.RO_DATA_OFFSET >= len(instructions), \
            "Памяти команд недостаточно для хранения программы"

        self.memory = instructions + [0] * (Offsets.RO_DATA_OFFSET - len(instructions))
        ro_data = get_ro_data(json_instructions)

        assert len(ro_data) < Offsets.DATA_OFFSET - Offsets.RO_DATA_OFFSET, \
            "Передан больший объем данных rodata, чем максимальный возможный (max 50)"
        self.memory += ro_data + [0] * (Offsets.MEMORY_GENERAL_SIZE - Offsets.RO_DATA_OFFSET - len(ro_data))

        self.stack = []
        self.return_stack = []
        self.stack_pointer = -1

        assert json_instructions["start_address"] < Offsets.RO_DATA_OFFSET, \
            "Точка входа в программу находится все сегмента кода"
        self.program_counter = json_instructions["start_address"]

        self.input_tokens = input_tokens
        self.output_tokens = []
        self._tick = 0

    def tick(self, amount=1):
        self._tick += amount

    def get_tick(self):
        return self._tick

    def latch_pc(self):
        self.program_counter += 1
        self.tick()
        if self.program_counter >= Offsets.RO_DATA_OFFSET:
            raise SegmentOverflow()

    def pop_stack(self):
        self.stack.pop()
        self.tick()
        self.stack_pointer -= 1
        self.tick()

    def push_end_to_stack(self):
        self.stack.append(0)
        self.tick()
        self.stack_pointer += 1
        self.tick()

    def get_next_instruction(self):
        return self.memory[self.program_counter - 1]

    def handle_write_signal(self):
        if self.input_tokens:
            value_to_push = []
            while True:
                self.tick()  # for if/else block
                if self.input_tokens:
                    symbol = self.input_tokens.pop(0)
                    self.tick()
                    self.tick()  # for if/else block
                    if symbol != " ":
                        value_to_push.append(symbol)
                        self.tick()
                    else:
                        ar_to_push = []
                        string_to_push = ''.join(value_to_push[::-1])
                        word_arr = string_to_push.encode()
                        ar_to_push.extend(word_arr)
                        ar_to_push.append(len(string_to_push))
                        logging.info('Word pushed: %s', ''.join(string_to_push))
                        self.stack.extend(ar_to_push)
                        self.tick()
                        self.stack_pointer += len(ar_to_push)
                        self.tick()
                        break
                else:
                    ar_to_push = []
                    string_to_push = ''.join(value_to_push[::-1])
                    word_arr = string_to_push.encode()
                    ar_to_push.extend(word_arr)
                    ar_to_push.append(len(string_to_push))
                    logging.info('Word pushed: %s', ''.join(string_to_push))
                    self.stack.extend(ar_to_push)
                    self.tick()
                    self.stack_pointer += len(ar_to_push)
                    self.tick()
                    break
        else:
            logging.info('End of input tokens')
            raise InputTokensEnd

    def handle_read_signal_digit(self):
        if self.stack:
            word = self.stack.pop()
            self.tick()
            self.stack_pointer -= 1
            self.tick()
            new_tokens = []
            try:
                symbols = str(word)
                for symbol in symbols:
                    logging.info('New output symbols: %s << %s',
                                 repr(''.join(new_tokens)), symbol)
                    new_tokens.append(symbol)
                    self.tick()
                self.output_tokens.append(''.join(new_tokens))
                logging.info('Output symbols: %s', repr(" ".join(self.output_tokens)))
            except TypeError:
                self.handle_read_signal_digit()

    def handle_read_signal_string(self):
        if self.stack:
            length = self.stack.pop()
            self.tick()
            self.stack_pointer -= 1
            self.tick()
            new_tokens = []
            for ind in range(length):
                letter_code = self.stack.pop()
                self.tick()
                symbol = chr(letter_code)
                try:
                    logging.info('New output symbols: %s << %s',
                                 repr(''.join(new_tokens)), symbol)
                    new_tokens.append(symbol)
                    self.tick()
                    self.stack_pointer -= 1
                    self.tick()
                except TypeError:
                    self.handle_read_signal_string()
            self.output_tokens.append(''.join(new_tokens))
            logging.info('Output symbols: %s', repr(" ".join(self.output_tokens)))

    def handle_signal(self, signal):
        if signal == Signal.WRITE:
            self.handle_write_signal()
            self.tick()
        elif signal == Signal.WRITE_ARG_STR:
            offset = int(self.stack.pop()) + Offsets.RO_DATA_OFFSET
            self.tick()  # pop
            self.tick()  # sum up
            length = self.memory[offset]
            self.tick()  # get from mem
            stri = self.memory[offset + length: offset:-1]
            self.tick(amount=length)  # iterate through array

            self.stack.extend(stri)
            self.tick()
            self.stack.append(self.memory[offset])
            self.tick()  # get from mem
            self.tick()  # append to stack
        elif signal == Signal.WRITE_ARG_DIG:
            offset = int(self.stack.pop()) + Offsets.RO_DATA_OFFSET
            self.tick()  # pop
            self.tick()  # sum up
            self.stack.append(self.memory[offset])
            self.tick()  # get from mem
            self.tick()  # offset
        elif signal == Signal.READ:
            self.handle_read_signal_digit()
            self.tick()
        elif signal == Signal.READ_STR:
            self.handle_read_signal_string()
            self.tick()
        elif signal == Signal.CHECK_GRT:
            self.tick()  # for if\else block
            if self.stack_pointer > 1:
                self.stack.append(1 if self.stack.pop() < self.stack.pop() else 0)
                self.tick()  # first pop
                self.tick()  # second pop
                self.tick()  # if else decision
                self.tick()  # append
                self.stack_pointer -= 1
                self.tick()
            else:
                self.stack.append(1)
                self.tick()
                self.stack_pointer += 1
                self.tick()
        elif signal == Signal.CHECK_LS:
            self.tick()  # for if\else block
            if self.stack_pointer > 1:
                self.stack.append(1 if self.stack.pop() > self.stack.pop() else 0)
                self.tick()  # first pop
                self.tick()  # second pop
                self.tick()  # if else decision
                self.tick()  # append
                self.stack_pointer -= 1
                self.tick()
            else:
                self.stack.append(1)
                self.tick()
                self.stack_pointer += 1
                self.tick()
        elif signal == Signal.CHECK_EQ:
            self.tick()  # for if block
            if self.stack_pointer > 1:
                self.stack.append(1 if self.stack.pop() == self.stack.pop() else 0)
                self.tick()  # first pop
                self.tick()  # second pop
                self.tick()  # if else decision
                self.tick()  # append
                self.stack_pointer -= 1
                self.tick()
            else:
                self.stack.append(1)
                self.tick()
                self.stack_pointer += 1
                self.tick()
        elif signal == Signal.CHECK_NOT_LAST:
            check_ans = 0 if len(self.stack) == 1 else 1
            self.tick()  # equality
            self.tick()  # if\else block
            self.stack.append(check_ans)
            self.tick()
            self.stack_pointer += 1
            self.tick()
        elif signal == Signal.DUPLICATE_SECOND:
            self.tick()  # for if block
            if self.stack_pointer > 0:
                self.tick()
                self.stack.append(self.stack[self.stack_pointer - 1])
                self.tick()  # minus
                self.tick()  # get value from stack
                self.tick()  # append
                self.stack_pointer += 1
                self.tick()
        elif signal == Signal.DUPLICATE:
            self.tick()  # for if block
            if self.stack_pointer > 0:
                self.tick()
                self.stack.append(self.stack[self.stack_pointer])
                self.tick()  # get value from stack
                self.tick()  # append
                self.stack_pointer += 1
                self.tick()
        elif signal == Signal.SWAP:
            self.stack.insert(self.stack_pointer - 1, self.stack[self.stack_pointer])
            self.tick()  # calculate stack ptr - 1
            self.tick()  # get value from stack
            self.tick()  # insert
            self.stack_pointer += 1
            self.tick()
        elif signal == Signal.ADD:
            self.stack.append(int(self.stack.pop()) + int(self.stack.pop()))
            self.tick()  # first pop
            self.tick()  # second pop
            self.tick()  # addition
            self.tick()  # append
            self.stack_pointer -= 1
            self.tick()
        elif signal == Signal.DROP:
            self.tick()  # for if block
            if self.stack_pointer > 0:
                self.stack.pop()
                self.tick()
                self.stack_pointer -= 1
                self.tick()
