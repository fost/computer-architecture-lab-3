#!/usr/bin/python3
# pylint: disable=missing-function-docstring
# pylint: disable=too-many-branches
# pylint: disable=line-too-long

"""Модуль, где идет запуск процессора и находится Control Unit
"""

import logging
import sys

from datapath import DataPath
from tools import Signal, Command, JUMP_MASK, INSTR_ADDRESS_MASK, HaltProgram, InputTokensEnd, SegmentOverflow, Offsets
from translator import read_instructions

logging.basicConfig(level=logging.INFO)


class ControlUnit:
    """Блок управления процессора. Выполняет декодирование инструкций и
        управляет состоянием процессора, включая обработку данных (DataPath).
        После декодирования посылает сигнал в DataPath
    """

    def __init__(self, data_path):
        self.data_path = data_path

    def send_signal(self, signal):
        self.data_path.handle_signal(signal)

    def start(self):
        cur_instr = 0
        limit = 1000
        instr_counter = 0
        try:
            while True:
                assert limit > instr_counter, "Достигнут лимит инструкций, программа остановлена"
                cur_instr =\
                    self.data_path.memory[Offsets.INSTRUCTIONS_OFFSET + self.data_path.program_counter]
                self.command_cycle()
                self.custom_log(cur_instr, self.data_path.program_counter - 1)
                instr_counter += 1
        except HaltProgram:
            pass
        self.custom_log(cur_instr, self.data_path.program_counter - 1)
        return " ".join(self.data_path.output_tokens), self.data_path._tick

    def command_cycle(self):
        try:
            self.data_path.latch_pc()
        except SegmentOverflow as segment_fault:
            logging.error("Программа пытается исполнить сегмент данных, аварийный останов")
            raise HaltProgram() from segment_fault
        decoded_instruction = self.data_path.get_next_instruction()
        if decoded_instruction == Command.HALT:
            raise HaltProgram()
        if decoded_instruction == Command.LST:
            self.send_signal(Signal.CHECK_LS)
        elif decoded_instruction == Command.GRT:
            self.send_signal(Signal.CHECK_GRT)
        elif decoded_instruction == Command.EQ:
            self.send_signal(Signal.CHECK_EQ)
        elif decoded_instruction == Command.LAST:
            self.send_signal(Signal.CHECK_NOT_LAST)
        elif decoded_instruction & JUMP_MASK == Command.JZ:
            self.data_path.tick()  # get value from stack
            self.data_path.tick()  # if block
            if not bool(self.data_path.stack[self.data_path.stack_pointer]):
                self.data_path.program_counter = Offsets.INSTRUCTIONS_OFFSET + \
                    int(decoded_instruction & INSTR_ADDRESS_MASK)
                self.data_path.tick()  # apply mask
                self.data_path.tick()  # add
            self.data_path.pop_stack()
        elif decoded_instruction & JUMP_MASK == Command.JUMP:
            self.data_path.program_counter = Offsets.INSTRUCTIONS_OFFSET + \
                int(decoded_instruction & INSTR_ADDRESS_MASK)
            self.data_path.tick()  # apply mask
            self.data_path.tick()  # add
        elif decoded_instruction & JUMP_MASK == Command.CALL:
            self.data_path.return_stack.append(self.data_path.program_counter)
            self.data_path.tick()
            self.data_path.program_counter = Offsets.INSTRUCTIONS_OFFSET + \
                int(decoded_instruction & INSTR_ADDRESS_MASK)
            self.data_path.tick()  # apply mask
            self.data_path.tick()  # add
        elif decoded_instruction & Command.PUSH_ARG_STR != 0:
            self.data_path.stack.append(int(decoded_instruction & INSTR_ADDRESS_MASK))
            self.data_path.tick()  # apply mask
            self.data_path.tick()  # append
            self.data_path.stack_pointer += 1
            self.data_path.tick()
            is_dig = decoded_instruction & JUMP_MASK == Command.PUSH_ARG
            self.data_path.tick()  # apply mask
            self.data_path.tick()  # compare
            try:
                self.send_signal(Signal.WRITE_ARG_DIG if is_dig else Signal.WRITE_ARG_STR)
                self.data_path.tick()  # if block
            except InputTokensEnd:
                self.data_path.push_end_to_stack()
        elif decoded_instruction == Command.PUSH:
            try:
                self.send_signal(Signal.WRITE)
            except InputTokensEnd:
                self.data_path.push_end_to_stack()
        elif decoded_instruction == Command.DUP_SEC:
            self.send_signal(Signal.DUPLICATE_SECOND)
        elif decoded_instruction == Command.DUP:
            self.send_signal(Signal.DUPLICATE)
        elif decoded_instruction == Command.SWAP:
            self.send_signal(Signal.SWAP)
        elif decoded_instruction == Command.ADD:
            self.send_signal(Signal.ADD)
        elif decoded_instruction == Command.RETURN:
            self.data_path.program_counter = Offsets.INSTRUCTIONS_OFFSET + \
                self.data_path.return_stack.pop()
            self.data_path.tick()
        elif decoded_instruction == Command.DROP:
            self.send_signal(Signal.DROP)
        elif decoded_instruction == Command.PRINT:
            self.send_signal(Signal.READ)
        elif decoded_instruction == Command.PRINT_STR:
            self.send_signal(Signal.READ_STR)

    def __repr__(self):
        top_element = self.data_path.stack[self.data_path.stack_pointer] \
            if self.data_path.stack_pointer != -1 else '-'
        ret = self.data_path.return_stack[len(self.data_path.return_stack) - 1] \
            if self.data_path.return_stack else '-'
        state = f"{{TICK: {self.data_path._tick}, PC: {self.data_path.program_counter}," \
                f" STACK POINTER: {self.data_path.stack_pointer}," \
                f" TOP ELEMENT: {top_element}," \
                f" RETURN STACK TOP EL: {ret} }}"
        return f"{state}"

    def custom_log(self, current_instruction, mem_address):
        decoded_instruction = current_instruction
        assert decoded_instruction >= 0, "Инструкция должна быть беззнаковым числом"
        assert decoded_instruction < 1024, "Инструкция должна быть десятибитной"
        if decoded_instruction <= Command.DROP.value:
            mnemonics = Command(decoded_instruction).name
            address = ''
        else:
            mnemonics = Command(int(decoded_instruction & JUMP_MASK)).name
            address = int(decoded_instruction & INSTR_ADDRESS_MASK)
            symbol = ''
        action = f"({bin(decoded_instruction)}) {mnemonics} {address} @ {mem_address})"
        logging.info('%s %s', action, self)
        logging.info('%s', self.data_path.stack)


def run_processor(instructions, input_tokens):
    data_path = DataPath(input_tokens, instructions)
    control_unit = ControlUnit(data_path)
    output, ticks = control_unit.start()
    print(output)
    print(ticks)

def main(args):
    assert len(args) == 2 or len(args) == 1, \
        "Wrong arguments: control_unit.py <binary code file> [<file with data>]"

    bin_code_file = args[0]
    data_file = args[1] if len(args) == 2 else None
    instructions = read_instructions(bin_code_file)
    input_tokens = []
    if data_file:
        with open(data_file, "r", encoding="utf-8") as file:
            data = file.read()
            for char in data:
                input_tokens.append(char)
    run_processor(instructions, input_tokens)


if __name__ == '__main__':
    main(sys.argv[1:])
