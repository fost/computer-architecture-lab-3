# Computer architecture lab 3


- Мусин Булат Юлдашевич P34312
- `forth | stack | neum | hw | tick | struct | stream | mem | pstr | prob2`

## Язык программирования

Форма Бэкуса — Наура:

``` ebnf
<program> ::= (<function>)+ (<term>)+

<term> ::= <comment> | <command>

<command> ::= "+" | "swap" | "." | "<<" | "last" | "dup_sec" | "drop" | <push>

<push> ::= ("S\" " [a-zA-Zа-яА-Я!?_]+ " \"") | [0-9]+

<bool_command> ::= "last"

<loop> ::= "begin" (<command>)+ (<bool_command> | <cond_op>) "while" (<command>)+ "repeat"

<cond_op> ::= ">" | "<" | "="

<name> ::= [a-zA-Zа-яА-Я!?_]+

<function> ::= ":" <name> <comment> (<command> | <loop> | <push>)+ ";"

<comment> ::= "(" [a-z]+ ")"
```
В начале программы идет объявление всех функций. После этого задается набор команд и функций, которые выполняются последовательно. Математические выражения представляются постфиксной записью при использовании стековой нотации. 

Отсутствует система типов. Нет возможности узнать, что лежит на вершине стека — число со знаком, число без знака, указатель на строку, символ, или два числа, рассматриваемых как одно длинное число. Контроль типов возлагается на программиста. 

Пояснение:

- `+` -- сложить два верхних элемента на стеке ( a b -- a+b )
- `last` -- проверка, является ли верхний элемент единственным в стеке
- `swap` -- дублировать верхний элемент за второй по счету элемент ( a b -- b a b)
- `dup` -- дублирует первый элемент ( b -- b b)
- `drop` -- удаляет верхний элемент со стека
- `.` -- достать и напечатать верхний элемент стека
- `<<` -- ввод элемента на верх стека из потока
- `<` -- проверка, является ли первый элемент в стеке меньше второго
- `>` -- проверка, является ли первый элемент в стеке больше второго
- `=` -- проверка на равенство первых двух элементов в стеке
- loop -- реализует бесконечный цикл с прерыванием, причем первая команда - это условие прерывания, последующие команды - команды в цикле. Пример: `begin dup 100 < while + repeat` Складывать элементы на верхушке стека до тех пор, пока верхний элемент стека меньше 100.
- function -- объявление функции. Пример: `: funcname ( func description ) + ;`
- comment -- все, что находится в круглых скобках
- name -- имя функции, состоит из символьных знаков
- push -- кладет указанное значение на вершину стека.


## Организация памяти

- В данной реализации основным местом хранения является стек. В стек записываются данные, получаемые с потока ввода, а также данные, которые должны быть выведены из основной памяти. 
- Константы поддерживаются. Для хранения констант используется сегмент rodata, доступ относительно начала сегмента 
- Переменную объявить нельзя, все переменные должны быть положены на стек до начала работы с ними через порт ввода. 
- Поддерживается объвление функций, причем все функции должнпы быть объявлены до входной точки программы.
- Переменные обладают локальной областью видимости, функции и константы - глобальной.
- Поддерживается работа исключительно с переменными и константами, отсутствует определения массивов, списков, ссылок, иных структур данных

Модель памяти процессора:

1. Память команд и данных общая. (Архитектура фон Неймана) Линейное адресное пространство.
- Сначала идет сегмент памяти команд. Размер 100. Машинное слово -- 10 бит, беззнаковое. Первые 3 бита зарезервированы для указания типа команды (адресная/нет). Адресация относительная. Реализуется списком строк, описывающих инструкции (одно слово -- одна ячейка).
- Затем идет сегмент памяти read only данных. Размер 50. Машинное слово -- 64 бита, беззнаковое.  Реализуется списком строк. Предназначен для хранения неизменяемых данных.
- Затем идет сегмент памяти данных. Размер 50. Машинное слово -- 64 бита, беззнаковое.  Реализуется списком строк. Сегмент данных предназначен для хранения глобальных переменных, в данной реализации процессора отсутствуют команды, которые работали бы с сегментом данных. 
2. Стек. Заменяет регистры. Машинное слово -- 64 бита, знаковое. Линейное адресное пространство. Реализуется списком строк и чисел (для служебных целей).

```text
Instruction and Data memory
+-----------------------------+
| 00  : function              |
| 01  : function              |
|    ...                      | 
| n   : program               |
| n+1 : program               |
|    ...                      |
| 99  : end of program space  |
+-----------------------------+
| 100 : row data              |
|    ...                      |
| 149 : end of row data       |
| 150 : data                  |
|    ...                      |
| 199 : end of data space     |
+-----------------------------+
```

## Система команд

Особенности процессора:

- Машинное слово команд -- 10 бит, беззнаковое. Машинное слово данных - 64 бит, беззнаковое.
- Память:
    - совмещенная (Архитектура фон Неймана). Сначала идет сегмент инструкций, затем сегмент read only данных, затем сегмент данных. У каждого сегмента фиксированная величина.
    - память данных адресуется с помощью относительной адресации относительно начала сегмента
    - память данных может быть записана read only данными при инициалиации Data Path
    - память данных может быть прочитана в стек `stack`
- Стек: `stack`:
    - хранит локальные переменные, таким образом является основным местом хранения данных
    - заменяет регистры: например, если требуется передать отсносительный адрес, он кладется на стек
    - верхние три элемента образуют TOS, что позволяет проводить операции над ними эффективно за один такт
    - может быть подан на вывод
    - может быть записан с порта ввода или из памяти данных
- Ввод-вывод -- порты ввода/вывода, токенизирован, символьный
- `program_counter` -- счётчик команд:
    - инкрементируется после каждой инструкции или перезаписывается инструкцией перехода
- `return stack` -- стак возвратов:
    - используется для поддержки вызова функций, хранит относительные адреса возврата
- `stack_pointer` -- указатель на верхний элемент:
    - инкрементируется/декрементируется после каждой инструкции перезаписывания стека

### Набор инструкции

| Syntax    | Mnemonic      | Кол-во тактов   | Comment                                                 |
|:----------|:--------------|-----------------|:--------------------------------------------------------|
| `+`       | add           | 5               | сложение двух значений с вершины стека                  |
| `>`       | grt           | 6               | больше ли первое значение в стеке второго? ответ в стек |
| `<`       | lst           | 6               | то же, что grt, только проверка на "меньше"             |
| `=`       | eq            | 6               | равны ли два числа на врешине стека? ответ в стек       |
| `swap`    | swap          | 4               | дублировать верхний элемент за второй по счету элемент  |
| `last`    | last          | 4               | проверка, не является ли число последним в стеке        |
| `.`       | print         | 4               | выводит верхнее значение в стеке                        |
| `type`    | type          | 6 + n           | выводит строку длиной n из стека                        |
| `<<`      | push          | 4 * n + 2       | читает значения (n штук) и заносит в стек               |
|           | ret           | 1               | вернуться из функции                                    |
| `dup_sec` | dup_sec       | 6               | дублирует второй сверху элемент на вершину стека        |
| `dup`     | dup           | 5               | дублирует верхний элемент                               |
| `drop`    | drop          | 3               | удаляет верхнее значение из стека                       |
|           | call `<addr>` | 3               | вызов функции                                           |
|           | jz `<addr>`   | 4 / 6 (if jump) | условный переход                                        |
|           | jmp `<addr>`  | 6               | безусловный переход                                     |
|           | push_arg      | 10              | добавление числа из программы в стек                    |
| `S"  "`   | push_arg_str  | 12 + n          | добавление строки длиной n из программы в стек          |
|           | halt          | 0               | завершение программы                                    |

Машинный код преобразуется в бинарный. Бинарный код - 10 бит, первые 3 бита зарезервированы для указания типа команд перехода.
Если команда адресная, то остальные 7 бит используются для адресации относительно начала сегмента

| Syntax    | Mnemonic      | Binary Code     | 
|:----------|:--------------|-----------------|
| `+`       | add           | 0000000001      | 
| `>`       | grt           | 0000000010      | 
| `<`       | lst           | 0000000011      |
| `=`       | eq            | 0000000100      | 
| `swap`    | swap          | 0000000101      |
| `last`    | last          | 0000000110      |
| `.`       | print         | 0000000111      |
| `type`    | type          | 0000001000      |
| `<<`      | push          | 0000001001      |
|           | ret           | 0000001010      |
| `dup_sec` | dup_sec       | 0000001011      |
| `dup`     | dup           | 0000001100      |
| `drop`    | drop          | 0000001101      |
|           | call `<addr>` | 001<7 bit addr> |
|           | jz `<addr>`   | 010<7 bit addr> |
|           | jmp `<addr>`  | 011<7 bit addr> |
|           | push_arg      | 100<7 bit addr> |
| `S"  "`   | push_arg_str  | 101<7 bit addr> |
|           | halt          | 0000000000      |

## Транслятор
         
Интерфейс командной строки: `translator.py <input_file> <target_file>"`

Реализовано в модуле: [translator](./translator.py)

Трансляция проходит в один этап (функция `translate`), во время которого:
1. Из текста убираются все незначимые слова, остаются лишь значимые термы
2. Идет проверка на корректность синтаксиса программы
3. Термы кодируются в бинарный вид, заносятся в json файл
4. Формируется мнемоника для удобочитаемости, тоже добавляются в json файл
5. Формируется список read only данных, которые будут записаны в секцию rodata

Правила генерации машинного кода:
- один символ языка -- одна инструкция;
- для команд, однозначно соответствующим инструкциям -- прямое отображение;
- для объявления функций (многоточие -- произвольный код)

| Номер команды/инструкции | Программа | Машинный код |
|:-------------------------|:----------|:-------------|
| n                        | `:`       |              |
| ...                      | ...       | ...          |
| n+3                      | `;`       | `ret`        |

- для циклов с условием (многоточие -- произвольный код)

| Номер команды/инструкции | Программа        | Машинный код |
|:-------------------------|:-----------------|:-------------|
| -                        | `begin`          | -            |
| n                        | `<bool_command>` | `jz (k+1)`   |
| -                        | `while`          | -            |
| ...                      | ...              | ...          |
| k                        | `repeat`         | `jmp (n)`    |
| k+1                      | ...              | ...          |

## Модель процессора

Реализован в двух модулях: [control_unit](./control_unit.py), [datapath](./datapath.py).

### Control Unit 

<img src="img/control_unit.png" alt="drawing" width="500"/>

Hardwired.
Моделирование на уровне инструкций.

Достает команду из памяти по относительному адресу, который хранится в Program Counter, декодирует ее. 
Декодирование происходит с помощью сопоставления кода и порядкового номера команды. Для адресных команд 
используется маска, позволяющая определить тип перехода и аргумент (относительный адрес). В зависимости от типа 
адресной команды, адрес возврата может быть положен в стек возврата (для команды call) или в 
program counter (для jmp, jz)

После декодирования инструкции в DataPath посылается один из сигналов:
```
class Signal(enum.IntEnum):
    WRITE = 0
    CHECK_LS = 1
    CHECK_GRT = 2
    CHECK_EQ = 3
    CHECK_NOT_LAST = 4
    SWAP = 5
    ADD = 6
    READ_STR = 7
    READ = 8
    DUPLICATE_SECOND = 9
    DUPLICATE = 10
    WRITE_ARG_STR = 11
    WRITE_ARG_DIG = 12
    DROP = 13
```
Когда была декодирована команда halt, работа процессора останавливается (реализовано с помощью прерывания)

### DataPath

<img src="img/datapath.jpg" alt="drawing" width="500"/>

Получает сигнал от CU и обрабатывает его.
В регистре stack pointer хранится указатель на верх стека, который изменяется после манипуляций со стеком.
С TOS связана память, откуда в стек могут записываться данные.
Также в TOS приходят данные с порта ввода, и из него выходят данные на порт выхода.

### Апробация

Интеграционные и модульные тесты в виде golden тестов реализованы тут: [testing.py](./testing.py)
В них проверяется корректность трансляции, выполнения алгоритмов prob2, cat, hello, а также корректная обработка сигналов в datapath,
проверка на генерацию корректных ошибок при некорректно заданной программе

CI:
```
stages:
  - test

test-job:
  stage: test
  image:
    name: python-tools
    entrypoint: [""]
  before_script:
    - pip install pytest-golden
  script:
    - echo "Running unit tests..."
    - python3-coverage run -m pytest --verbose
    - find . -type f -name "*.py" | xargs -t python3-coverage report
    - find . -type f -name "*.py" | xargs -t pep8 --ignore=E501
    - find . -type f -name "*.py" | xargs -t pylint --disable=R1702,R0902,C0200,R0912,R0915,R0914,C0103,C0116
```
где:

- `python3-coverage` -- формирование отчёта об уровне покрытия исходного кода.
- `pytest-golden` -- утилита для запуска golden тестов.
- `pep8` -- утилита для проверки форматирования кода. 
- `pylint` -- утилита для проверки качества кода. Некоторые правила отключены в отдельных модулях, это обсуловлено контекстом.

Пример использования и журнал работы процессора на примере `cat`:

```
> cat test-programs/cat.fth 
begin << dup while type repeat
> cat in/cat.in 
This string should print out unchanged 
> python3.11 translator.py test-programs/cat.fth out/cat.json
> cat out/cat.json
{
  "program": [
    {
      "address": 0,
      "opcode": "0000001001",
      "mnemonic": "<<",
      "parameter": null
    },
    {
      "address": 1,
      "opcode": "0000001100",
      "mnemonic": "dup",
      "parameter": null
    },
    {
      "address": 2,
      "opcode": "0100000101",
      "mnemonic": "jz 5",
      "parameter": null
    },
    {
      "address": 3,
      "opcode": "0000001000",
      "mnemonic": "type",
      "parameter": null
    },
    {
      "address": 4,
      "opcode": "0110000000",
      "mnemonic": "jmp 0",
      "parameter": null
    },
    {
      "address": 5,
      "opcode": "0000000000",
      "mnemonic": "halt",
      "parameter": null
    }
  ],
  "start_address": 0
}
>  python3.11 control_unit.py out/cat.json in/cat.in
INFO:root:Word pushed: sihT
INFO:root:(0b1001) PUSH  @ 0) {TICK: 23, PC: 1, STACK POINTER: 4, TOP ELEMENT: 4, RETURN STACK TOP EL: - }
INFO:root:[115, 105, 104, 84, 4]
INFO:root:(0b1100) DUP  @ 1) {TICK: 29, PC: 2, STACK POINTER: 5, TOP ELEMENT: 4, RETURN STACK TOP EL: - }
INFO:root:[115, 105, 104, 84, 4, 4]
INFO:root:(0b100000101) JZ 5 @ 2) {TICK: 34, PC: 3, STACK POINTER: 4, TOP ELEMENT: 4, RETURN STACK TOP EL: - }
INFO:root:[115, 105, 104, 84, 4]
INFO:root:New output symbols: '' << T
INFO:root:New output symbols: 'T' << h
INFO:root:New output symbols: 'Th' << i
INFO:root:New output symbols: 'Thi' << s
INFO:root:Output symbols: 'This'
INFO:root:(0b1000) PRINT_STR  @ 3) {TICK: 50, PC: 4, STACK POINTER: -1, TOP ELEMENT: -, RETURN STACK TOP EL: - }
INFO:root:[]
INFO:root:(0b110000000) JUMP 0 @ -1) {TICK: 53, PC: 0, STACK POINTER: -1, TOP ELEMENT: -, RETURN STACK TOP EL: - }
INFO:root:[]
INFO:root:Word pushed: gnirts
INFO:root:(0b1001) PUSH  @ 0) {TICK: 84, PC: 1, STACK POINTER: 6, TOP ELEMENT: 6, RETURN STACK TOP EL: - }
INFO:root:[103, 110, 105, 114, 116, 115, 6]
INFO:root:(0b1100) DUP  @ 1) {TICK: 90, PC: 2, STACK POINTER: 7, TOP ELEMENT: 6, RETURN STACK TOP EL: - }
INFO:root:[103, 110, 105, 114, 116, 115, 6, 6]
INFO:root:(0b100000101) JZ 5 @ 2) {TICK: 95, PC: 3, STACK POINTER: 6, TOP ELEMENT: 6, RETURN STACK TOP EL: - }
INFO:root:[103, 110, 105, 114, 116, 115, 6]
INFO:root:New output symbols: '' << s
INFO:root:New output symbols: 's' << t
INFO:root:New output symbols: 'st' << r
INFO:root:New output symbols: 'str' << i
INFO:root:New output symbols: 'stri' << n
INFO:root:New output symbols: 'strin' << g
INFO:root:Output symbols: 'This string'
INFO:root:(0b1000) PRINT_STR  @ 3) {TICK: 117, PC: 4, STACK POINTER: -1, TOP ELEMENT: -, RETURN STACK TOP EL: - }
INFO:root:[]
INFO:root:(0b110000000) JUMP 0 @ -1) {TICK: 120, PC: 0, STACK POINTER: -1, TOP ELEMENT: -, RETURN STACK TOP EL: - }
INFO:root:[]
INFO:root:Word pushed: dluohs
INFO:root:(0b1001) PUSH  @ 0) {TICK: 151, PC: 1, STACK POINTER: 6, TOP ELEMENT: 6, RETURN STACK TOP EL: - }
INFO:root:[100, 108, 117, 111, 104, 115, 6]
INFO:root:(0b1100) DUP  @ 1) {TICK: 157, PC: 2, STACK POINTER: 7, TOP ELEMENT: 6, RETURN STACK TOP EL: - }
INFO:root:[100, 108, 117, 111, 104, 115, 6, 6]
INFO:root:(0b100000101) JZ 5 @ 2) {TICK: 162, PC: 3, STACK POINTER: 6, TOP ELEMENT: 6, RETURN STACK TOP EL: - }
INFO:root:[100, 108, 117, 111, 104, 115, 6]
INFO:root:New output symbols: '' << s
INFO:root:New output symbols: 's' << h
INFO:root:New output symbols: 'sh' << o
INFO:root:New output symbols: 'sho' << u
INFO:root:New output symbols: 'shou' << l
INFO:root:New output symbols: 'shoul' << d
INFO:root:Output symbols: 'This string should'
INFO:root:(0b1000) PRINT_STR  @ 3) {TICK: 184, PC: 4, STACK POINTER: -1, TOP ELEMENT: -, RETURN STACK TOP EL: - }
INFO:root:[]
INFO:root:(0b110000000) JUMP 0 @ -1) {TICK: 187, PC: 0, STACK POINTER: -1, TOP ELEMENT: -, RETURN STACK TOP EL: - }
INFO:root:[]
INFO:root:Word pushed: tnirp
INFO:root:(0b1001) PUSH  @ 0) {TICK: 214, PC: 1, STACK POINTER: 5, TOP ELEMENT: 5, RETURN STACK TOP EL: - }
INFO:root:[116, 110, 105, 114, 112, 5]
INFO:root:(0b1100) DUP  @ 1) {TICK: 220, PC: 2, STACK POINTER: 6, TOP ELEMENT: 5, RETURN STACK TOP EL: - }
INFO:root:[116, 110, 105, 114, 112, 5, 5]
INFO:root:(0b100000101) JZ 5 @ 2) {TICK: 225, PC: 3, STACK POINTER: 5, TOP ELEMENT: 5, RETURN STACK TOP EL: - }
INFO:root:[116, 110, 105, 114, 112, 5]
INFO:root:New output symbols: '' << p
INFO:root:New output symbols: 'p' << r
INFO:root:New output symbols: 'pr' << i
INFO:root:New output symbols: 'pri' << n
INFO:root:New output symbols: 'prin' << t
INFO:root:Output symbols: 'This string should print'
INFO:root:(0b1000) PRINT_STR  @ 3) {TICK: 244, PC: 4, STACK POINTER: -1, TOP ELEMENT: -, RETURN STACK TOP EL: - }
INFO:root:[]
INFO:root:(0b110000000) JUMP 0 @ -1) {TICK: 247, PC: 0, STACK POINTER: -1, TOP ELEMENT: -, RETURN STACK TOP EL: - }
INFO:root:[]
INFO:root:Word pushed: tuo
INFO:root:(0b1001) PUSH  @ 0) {TICK: 266, PC: 1, STACK POINTER: 3, TOP ELEMENT: 3, RETURN STACK TOP EL: - }
INFO:root:[116, 117, 111, 3]
INFO:root:(0b1100) DUP  @ 1) {TICK: 272, PC: 2, STACK POINTER: 4, TOP ELEMENT: 3, RETURN STACK TOP EL: - }
INFO:root:[116, 117, 111, 3, 3]
INFO:root:(0b100000101) JZ 5 @ 2) {TICK: 277, PC: 3, STACK POINTER: 3, TOP ELEMENT: 3, RETURN STACK TOP EL: - }
INFO:root:[116, 117, 111, 3]
INFO:root:New output symbols: '' << o
INFO:root:New output symbols: 'o' << u
INFO:root:New output symbols: 'ou' << t
INFO:root:Output symbols: 'This string should print out'
INFO:root:(0b1000) PRINT_STR  @ 3) {TICK: 290, PC: 4, STACK POINTER: -1, TOP ELEMENT: -, RETURN STACK TOP EL: - }
INFO:root:[]
INFO:root:(0b110000000) JUMP 0 @ -1) {TICK: 293, PC: 0, STACK POINTER: -1, TOP ELEMENT: -, RETURN STACK TOP EL: - }
INFO:root:[]
INFO:root:Word pushed: degnahcnu
INFO:root:(0b1001) PUSH  @ 0) {TICK: 334, PC: 1, STACK POINTER: 9, TOP ELEMENT: 9, RETURN STACK TOP EL: - }
INFO:root:[100, 101, 103, 110, 97, 104, 99, 110, 117, 9]
INFO:root:(0b1100) DUP  @ 1) {TICK: 340, PC: 2, STACK POINTER: 10, TOP ELEMENT: 9, RETURN STACK TOP EL: - }
INFO:root:[100, 101, 103, 110, 97, 104, 99, 110, 117, 9, 9]
INFO:root:(0b100000101) JZ 5 @ 2) {TICK: 345, PC: 3, STACK POINTER: 9, TOP ELEMENT: 9, RETURN STACK TOP EL: - }
INFO:root:[100, 101, 103, 110, 97, 104, 99, 110, 117, 9]
INFO:root:New output symbols: '' << u
INFO:root:New output symbols: 'u' << n
INFO:root:New output symbols: 'un' << c
INFO:root:New output symbols: 'unc' << h
INFO:root:New output symbols: 'unch' << a
INFO:root:New output symbols: 'uncha' << n
INFO:root:New output symbols: 'unchan' << g
INFO:root:New output symbols: 'unchang' << e
INFO:root:New output symbols: 'unchange' << d
INFO:root:Output symbols: 'This string should print out unchanged'
INFO:root:(0b1000) PRINT_STR  @ 3) {TICK: 376, PC: 4, STACK POINTER: -1, TOP ELEMENT: -, RETURN STACK TOP EL: - }
INFO:root:[]
INFO:root:(0b110000000) JUMP 0 @ -1) {TICK: 379, PC: 0, STACK POINTER: -1, TOP ELEMENT: -, RETURN STACK TOP EL: - }
INFO:root:[]
INFO:root:End of input tokens
INFO:root:(0b1001) PUSH  @ 0) {TICK: 382, PC: 1, STACK POINTER: 0, TOP ELEMENT: 0, RETURN STACK TOP EL: - }
INFO:root:[0]
INFO:root:(0b1100) DUP  @ 1) {TICK: 384, PC: 2, STACK POINTER: 0, TOP ELEMENT: 0, RETURN STACK TOP EL: - }
INFO:root:[0]
INFO:root:(0b100000101) JZ 5 @ 4) {TICK: 391, PC: 5, STACK POINTER: -1, TOP ELEMENT: -, RETURN STACK TOP EL: - }
INFO:root:[]
INFO:root:(0b0) HALT  @ 5) {TICK: 392, PC: 6, STACK POINTER: -1, TOP ELEMENT: -, RETURN STACK TOP EL: - }
INFO:root:[]
This string should print out unchanged
392
```


| ФИО         | алг.  | LoC | code инстр. | инстр. | такт. | вариант                                                        |
|-------------|-------|-----|-------------|--------|-------|----------------------------------------------------------------|
| Мусин Б. Ю. | hello | 1   | 3           | 3      | 66    | forth, stack, neum, hw, tick, struct, stream, mem, pstr, prob2 |
| Мусин Б. Ю. | cat   | 1   | 6           | 34     | 392   | forth, stack, neum, hw, tick, struct, stream, mem, pstr, prob2 |
| Мусин Б. Ю. | prob2 | 7   | 28          | 276    | 1321  | forth, stack, neum, hw, tick, struct, stream, mem, pstr, prob2 |