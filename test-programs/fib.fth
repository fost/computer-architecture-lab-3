: fib_next ( a b -- b a+b ) swap + ;
: fib_even_top ( a b -- b a+2b 2a+3b ) swap fib_next fib_next fib_next ;
: get_even_fib ( a b -- even numbers from fib up to 4000000 ) begin dup 4000000 < while fib_even_top repeat drop drop ;
: get_sum ( sum of entire stack ) begin last while + repeat ;
1 2
get_even_fib
get_sum .